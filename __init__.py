from trytond.pool import Pool
from .health_party import *

def register():
    Pool.register(
        Party,
        PatientSESAssessment,
        GnuHealthPatient,
        module='health_party_fiuner', type_='model')
